# sshd

A brief description of the role goes here.

## Requirements

None

## Role Variables

```
cts_role_sshd:
  PermitRootLogin: 'without-password'
  ForwardAgent: 'no'
  GSSAPIAuthentication: 'no'
```

## Dependencies

none

## Example Playbook

```
- hosts: all
  gather_facts: False
  pre_tasks:
    - name: pre_tasks | setup python
      raw: command -v yum >/dev/null && yum -y install python python-simplejson libselinux-python redhat-lsb || true ; command -v apt-get >/dev/null && sed -i '/cdrom/d' /etc/apt/sources.list && apt-get update && apt-get install -y python python-simplejson lsb-release aptitude || true
      changed_when: False
    - name: pre_tasks | gets facts
      setup:
      tags:
      - always

  roles:
    - sshd
```

